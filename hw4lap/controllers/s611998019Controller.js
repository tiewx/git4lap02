
const controller = {};
const { validationResult } = require('express-validator');


controller.list = (req,res) => {
  if(req.session.user){
    req.getConnection((err,conn) => {
      conn.query('SELECT s.id as sid , s.tiew as stiew , s.somwan as ssomwan , s.D611998019 as sD611998019 , n.id as nid , n.tle as ntle ,n.chaiboonrueang as nchaiboonrueang,n.D611998011 as nD611998011 FROM saran as s left JOIN naruechai as n ON s.id_naruechai = n.id ;',(err,xx) => {
        if(err){
          res.json(err);
        }else {
          res.render('s611998019/index',{
            data:xx,
            session: req.session
        });
        }
      });
    });
  }else {
    res.redirect('/');
  }
};

controller.add = (req,res) => {
  const data=req.body;
  if (data.id_naruechai=="") {
    data.id_naruechai=null;
  }
  console.log(data.id_naruechai);
  const errors = validationResult(req);
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      res.redirect('/s611998019/new');
    }else {
      req.session.success=true;
      req.session.topic="เพิ่มข้อมูลสำเร็จ";
      if(req.session.user){
        req.getConnection((err,conn) =>{
          conn.query('INSERT INTO saran set ?',[data],(err,xx) =>{
            if(err){
              res.json(err);
            }

            console.log(xx);
            res.redirect('/s611998019');
          });
        });
      }else {
        res.redirect('/');
      }
    }
};

controller.save =(req,res) =>{
  console.log(req.body);
  const data=req.body;
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('INSERT INTO saran set ?',[data],(err,xx) =>{
        if(err){
          res.json(err);
        }

        console.log(xx);
        res.redirect('/s611998019');
      });
    });
  }else {
    res.redirect('/');
  }
};

controller.delete =(req,res) =>{
  const {id} =req.params;
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('DELETE FROM saran WHERE id = ?',[id],(err,xx) =>{
        if(err){
          res.json(err);
        }
        console.log(xx);
        res.redirect('/s611998019');
      });
    });
  }else {
    res.redirect('/');
  }
};

controller.delete2 =(req,res) =>{
  const {id} =req.params;

  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('DELETE FROM saran WHERE id = ?',[id],(err,xx) =>{
        if(err){
          const  errorss= {errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }]}
          req.session.errors=errorss;
          req.session.success=false;
        }
        console.log(xx);
        req.session.success=true;
        req.session.topic="ลบสำเร็จ";
        res.redirect('/s611998019');
      });
    });
  }else {
    res.redirect('/');
  }
};

controller.del =(req,res) =>{
  const {id} =req.params;
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('SELECT s.id as sid , s.tiew as stiew , s.somwan as ssomwan , s.D611998019 as sD611998019 , n.id as nid , n.tle as ntle ,n.chaiboonrueang as nchaiboonrueang FROM saran as s left JOIN naruechai as n ON s.id_naruechai = n.id HAVING sid= ?',[id],(err,xx) =>{
        if(err){
          res.json(err);
        }
        console.log(xx);

        res.render('s611998019/formDelete',{
                data:xx[0],
                session: req.session

        });
      });
    });
  }else {
    res.redirect('/');
  }
};

controller.del2 =(req,res) =>{
  const {id} =req.params;
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('SELECT s.id as sid , s.tiew as stiew , s.somwan as ssomwan , s.D611998019 as sD611998019 , n.id as nid , n.tle as ntle ,n.chaiboonrueang as nchaiboonrueang FROM saran as s left JOIN naruechai as n ON s.id_naruechai = n.id HAVING sid= ?',[id],(err,xx) =>{
        if(err){
          res.json(err);
        }
        console.log(xx);
        res.render('s611998019/formDelete',{
                data:xx[0],
                session: req.session

        });
      });
    });
  }else {
    res.redirect('/');
  }
};


controller.edit =(req,res) =>{
  const {id} =req.params;
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM saran where id = ?',[id],(err,xx) =>{
        if(err){
          res.json(err);
        }
        console.log(xx);
        res.render('s611998019/form',{
                data:xx[0],
                session: req.session
        });
      });
    });
  }else {
    res.redirect('/');
  }
};


controller.edit2 =(req,res) =>{
  const {id} =req.params;
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM saran where id = ?',[id],(err,saranda) =>{
        if(err){
          res.json(err);
        }
        conn.query('SELECT * FROM naruechai;',(err,naruechaida) => {
          if(err){
            res.json(err);
          }

          res.render('s611998019/form',{
                  data:saranda[0],
                  data2:naruechaida,
                  idx:id,
                  session: req.session
          });
        });
      });
    });
  }else {
    res.redirect('/');
  }
};

controller.up = (req,res) => {
  const errors = validationResult(req);
  const {id} =req.params;
  const data=req.body;
  if (data.id_naruechai=="") {
    data.id_naruechai=null;
  }
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      if(req.session.user){
        req.getConnection((err,conn) =>{
          conn.query('SELECT * FROM saran where id = ?',[id],(err,xx) =>{
            if(err){
              res.json(err);
            }
            console.log(xx);
            res.render('s611998019/form',{
                    data:xx[0],
                    session: req.session
            });
          });
        });
      }else {
        res.redirect('/');
      }
    }else {
      req.session.success=true;
      req.session.topic="เพิ่มข้อมูลสำเร็จ";
      if(req.session.user){
        req.getConnection((err,conn) =>{
          conn.query('UPDATE saran SET ? WHERE id = ?',[data,id],(err,xx) =>{
            if(err){
              res.json(err);
            }
            console.log(xx);
            res.redirect('/s611998019');
          });
        });
      }else {
        res.redirect('/');
      }
    }
};

controller.up2 = (req,res) => {
  const errors = validationResult(req);
  const {id} =req.params;
  const data=req.body;
  if (data.id_naruechai=="") {
    data.id_naruechai=null;
  }
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      if(req.session.user){
        req.getConnection((err,conn) =>{
          conn.query('SELECT * FROM saran where id = ?',[id],(err,saranda) =>{
            if(err){
              res.json(err);
            }
            conn.query('SELECT * FROM naruechai;',(err,naruechaida) => {
              if(err){
                res.json(err);
              }
              res.render('s611998019/form',{
                      data:saranda[0],
                      data2:naruechaida,
                      idx:id,
                      session: req.session
              });
            });
          });
        });
      }else {
        res.redirect('/');
      }
    }else {
      req.session.success=true;
      req.session.topic="เพิ่มข้อมูลสำเร็จ";
      if(req.session.user){
        req.getConnection((err,conn) =>{
          conn.query('UPDATE saran SET ? WHERE id = ?',[data,id],(err,xx) =>{
            if(err){
              res.json(err);
            }
            console.log(xx);
            res.redirect('/s611998019');
          });
        });
      }else {
        res.redirect('/');
      }
    }
};

controller.update =(req,res) =>{
  const {id} =req.params;
  const data=req.body;
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('UPDATE saran SET ? WHERE id = ?',[data,id],(err,xx) =>{
        if(err){
          res.json(err);
        }
        console.log(xx);
        res.redirect('/s611998019');
      });
    });
  }else {
    res.redirect('/');
  }
};

controller.new =(req,res) =>{
  const data=null;
  if(req.session.user){
    res.render('s611998019/form',{
      data:data,
      session: req.session
    });
  }else {
    res.redirect('/');
  }
};

controller.new2 =(req,res) =>{
  const data=null;
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM naruechai;',(err,nn) => {
        res.render('s611998019/form',{
          data:null,
          data2:nn,
          session: req.session
        });
      });
    });
  }else {
    res.redirect('/');
  }
};

controller.newsx =(req,res) =>{
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM saran;',(err,teacherdata) => {
        res.render('studentForm',{data:teacherdata});
      });
    });
  }else {
    res.redirect('/');
  }
};

module.exports = controller;
