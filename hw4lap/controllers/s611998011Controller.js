
const controller = {};
const { validationResult } = require('express-validator');


controller.list = (req,res) => {
  if(req.session.user){
    req.getConnection((err,conn) => {
      conn.query('SELECT s.id as sid , s.tle as stle , s.chaiboonrueang as schaiboonrueang , s.D611998011 as sD611998011 , n.id as nid , n.sam as nsam ,n.saeoung as nsaeoung, n.D631998035 as nD631998035 FROM naruechai as s left JOIN noppadol as n ON s.id_noppadol = n.id ;',(err,xx) => {
        if(err){
          res.json(err);
        }else {
          res.render('s611998011/naruechai',{
            data:xx,
            session: req.session
        });
        }
      });
    });
  }else {
    res.redirect('/');
  }
};

controller.add = (req,res) => {
  const data=req.body;
  if (data.id_noppadol=="") {
    data.id_noppadol=null;
  }
  const errors = validationResult(req);
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      res.redirect('/s611998011/new');
    }else {
      req.session.success=true;
      req.session.topic="เพิ่มข้อมูลสำเร็จ";
      if(req.session.user){
        req.getConnection((err,conn) =>{
          conn.query('INSERT INTO naruechai set ?',[data],(err,xx) =>{
            if(err){
              res.json(err);
            }

            console.log(xx);
            res.redirect('/s611998011');
          });
        });
      }else {
        res.redirect('/');
      }
    }
};

controller.save =(req,res) =>{
  console.log(req.body);
  const data=req.body;
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('INSERT INTO naruechai set ?',[data],(err,xx) =>{
        if(err){
          res.json(err);
        }

        console.log(xx);
        res.redirect('/s611998011');
      });
    });
  }else {
    res.redirect('/');
  }
};

controller.delete =(req,res) =>{
  const {id} =req.params;
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('DELETE FROM naruechai WHERE id = ?',[id],(err,xx) =>{
        if(err){
          const  errorss= {errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }]}
          req.session.errors=errorss;
          req.session.success=false;
        }
        console.log(xx);
        req.session.success=true;
        req.session.topic="ลบสำเร็จ";
        res.redirect('/s611998011');
      });
    });
  }else {
    res.redirect('/');
  }
};

controller.delete2 =(req,res) =>{
  const {id} =req.params;
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('DELETE FROM naruechai WHERE id = ?',[id],(err,xx) =>{
        if(err){
          const  errorss= {errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }]}
          req.session.errors=errorss;
          req.session.success=false;
        }
        console.log(xx);
        req.session.success=true;
        req.session.topic="ลบสำเร็จ";
        res.redirect('/s611998011');
      });
    });
  }else {
    res.redirect('/');
  }
};

controller.del =(req,res) =>{
  const {id} =req.params;
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('SELECT s.id as sid , s.tle as stle , s.chaiboonrueang as schaiboonrueang , s.D611998011 as sD611998011 , n.id as nid , n.sam as nsam ,n.saeoung as nsaeoung FROM naruechai as s left JOIN noppadol as n ON s.id_noppadol = n.id HAVING sid= ?',[id],(err,xx) =>{
        if(err){
          res.json(err);
        }
        console.log(xx);

        res.render('s611998011/naruechaiFormDelete',{
                data:xx[0],
                session: req.session

        });
      });
    });
  }else {
    res.redirect('/');
  }
};

controller.del2 =(req,res) =>{
  const {id} =req.params;
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('SELECT s.id as sid , s.tle as stle , s.chaiboonrueang as schaiboonrueang , s.D611998011 as sD611998011 , n.id as nid , n.sam as nsam ,n.saeoung as nsaeoung FROM naruechai as s left JOIN noppadol as n ON s.id_noppadol = n.id HAVING sid= ?',[id],(err,xx) =>{
        if(err){
          res.json(err);
        }
        console.log(xx);
        res.render('s611998011/naruechaiFormDelete',{
                data:xx[0],
                session: req.session

        });
      });
    });
  }else {
    res.redirect('/');
  }
};


controller.edit =(req,res) =>{
  const {id} =req.params;
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM naruechai where id = ?',[id],(err,xx) =>{
        if(err){
          res.json(err);
        }
        console.log(xx);
        res.render('s611998011/naruechaiForm',{
                data:xx[0],
                session: req.session
        });
      });
    });
  }else {
    res.redirect('/');
  }
};


controller.edit2 =(req,res) =>{
  const {id} =req.params;
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM naruechai where id = ?',[id],(err,naruechai) =>{
        if(err){
          res.json(err);
        }
        conn.query('SELECT * FROM noppadol;',(err,noppadol) => {
          if(err){
            res.json(err);
          }

          res.render('s611998011/naruechaiForm',{
                  data:naruechai[0],
                  data2:noppadol,
                  idx:id,
                  session: req.session
          });
        });
      });
    });
  }else {
    res.redirect('/');
  }
};

controller.up = (req,res) => {
  const errors = validationResult(req);
  const {id} =req.params;
  const data=req.body;
  if (data.id_noppadol=="") {
    data.id_noppadol=null;
  }
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      if(req.session.user){
        req.getConnection((err,conn) =>{
          conn.query('SELECT * FROM naruechai where id = ?',[id],(err,xx) =>{
            if(err){
              res.json(err);
            }
            console.log(xx);
            res.render('s611998011/naruechaiForm',{
                    data:xx[0],
                    session: req.session
            });
          });
        });
      }else {
        res.redirect('/');
      }
    }else {
      req.session.success=true;
      req.session.topic="เพิ่มข้อมูลสำเร็จ";
      if(req.session.user){
        req.getConnection((err,conn) =>{
          conn.query('UPDATE naruechai SET ? WHERE id = ?',[data,id],(err,xx) =>{
            if(err){
              res.json(err);
            }
            console.log(xx);
            res.redirect('/s611998011');
          });
        });
      }else {
        res.redirect('/');
      }
    }
};

controller.up2 = (req,res) => {
  const errors = validationResult(req);
  const {id} =req.params;
  const data=req.body;
  if (data.id_noppadol=="") {
    data.id_noppadol=null;
  }
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      if(req.session.user){
        req.getConnection((err,conn) =>{
          conn.query('SELECT * FROM naruechai where id = ?',[id],(err,naruechai) =>{
            if(err){
              res.json(err);
            }
            conn.query('SELECT * FROM noppadol;',(err,noppadol) => {
              if(err){
                res.json(err);
              }
              res.render('s611998011/naruechaiForm',{
                      data:naruechai[0],
                      data2:noppadol,
                      idx:id,
                      session: req.session
              });
            });
          });
        });
      }else {
        res.redirect('/');
      }
    }else {
      req.session.success=true;
      req.session.topic="เพิ่มข้อมูลสำเร็จ";
      if(req.session.user){
        req.getConnection((err,conn) =>{
          conn.query('UPDATE naruechai SET ? WHERE id = ?',[data,id],(err,xx) =>{
            if(err){
              res.json(err);
            }
            console.log(xx);
            res.redirect('/s611998011');
          });
        });
      }else {
        res.redirect('/');
      }
    }
};

controller.update =(req,res) =>{
  const {id} =req.params;
  const data=req.body;
  if (data.id_noppadol=="") {
    data.id_noppadol=null;
  }
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('UPDATE naruechai SET ? WHERE id = ?',[data,id],(err,xx) =>{
        if(err){
          res.json(err);
        }
        console.log(xx);
        res.redirect('/s611998011');
      });
    });
  }else {
    res.redirect('/');
  }
};

controller.new =(req,res) =>{
  const data=null;
  if(req.session.user){
    res.render('s611998019/naruechaiForm',{
      data:data,
      session: req.session
    });
  }else {
    res.redirect('/');
  }
};

controller.new2 =(req,res) =>{
  const data=null;
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM noppadol;',(err,nn) => {
        res.render('s611998011/naruechaiForm',{
          data:null,
          data2:nn,
          session: req.session
        });
      });
    });
  }else {
    res.redirect('/');
  }
};

controller.newsx =(req,res) =>{
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM naruechai;',(err,teacherdata) => {
        res.render('studentForm',{data:teacherdata});
      });
    });
  }else {
    res.redirect('/');
  }
};

module.exports = controller;
