
const controller ={};
const { validationResult } = require('express-validator');


controller.list=(req,res) => {
  req.getConnection((err,conn) =>{
    conn.query('SELECT s.id as sid,s.ploy as sploy, s.ratchawong as sratchawong, s.d601998023 as sd601998023, t.tiew as ttiew, t.somwan as tsomwan, t.d611998019 as td61198019  FROM jutarat as s  left join saran as t on s.id_saran = t.id',(err,s601998023s) =>{
      if (err) {
        res.json(err);
      }
      res.render('s601998023/s601998023s',{ //หาหน้า views ต้องใส่ / ด้วยเพราะมันหนาใน view ไม่เจอ
        data:s601998023s,
        session: req.session
      });
    });
  });
};

controller.save=(req,res)=>{
const data=req.body;

if (data.id_saran=="") {
  data.id_saran=null;
}
const errors = validationResult(req);
req.getConnection((err,conn)=>{
  conn.query('insert into jutarat set?',[data],(err,s601998023s) =>{
    if (!errors.isEmpty()) {
      req.session.errors=errors;
      req.session.success=false;
      res.redirect('/s601998023/new')

    }else {
      req.session.success=true;
      req.session.topic='เพิ่มข้อมูลสำเร็จ';

      res.redirect('/s601998023');
    }; // save ใน customer ไปเลย
  });
});
};

controller.delete=(req,res)=>{
const { id }=req.params;
req.getConnection((err,conn)=>{
  conn.query('select * from jutarat where id= ?',[id],(err,s601998023s) =>{
    if (err) {
      const  errorss= {errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }]}
      req.session.errors=errorss;
      req.session.success=false;
    }
    req.session.success=true;
    req.session.topic="ลบสำเร็จ";
    res.render('../views/s601998023/deleteForm',{
      session: req.session,
      data:s601998023s[0]
    });
  });
});
};

controller.edit=(req,res)=>{
const { id }=req.params;
req.getConnection((err,conn)=>{
  conn.query('Select * from jutarat where id = ?',[id],(err,jutaratdata) =>{
    conn.query('SELECT * FROM saran ',(err,sarandata)=>{res.render
            ('s601998023/jutaratFormUpdate',{
            session: req.session,
            data1:jutaratdata[0],
            data2:sarandata});
        });
      });
    });
};

controller.update = (req,res) => {
  const {id} = req.params;
  const data = req.body;

  if (data.id_saran=="") {
    data.id_saran=null;
  }

  const errors = validationResult(req);
  console.log(data);
      if(!errors.isEmpty()){
        req.session.errors=errors;
        req.session.success=false;
        req.getConnection((err,conn)=>{
          conn.query('Select * from jutarat where id = ?',[id],(err,jutaratdata) =>{
            conn.query('SELECT * FROM saran ',(err,sarandata)=>{res.render
                    ('s601998023/jutaratFormUpdate',{
                    session: req.session,
                    data1:jutaratdata[0],
                    data2:sarandata});
                });
              });
            });
      }else {
        req.session.success=true;
        req.session.topic="แก้ไขเรียบร้อย";
        req.getConnection((err,conn) =>{
          conn.query('update jutarat set ? where id = ?',[data,id],(err,s601998023s)=> {
            if(err){
              res.json(err);
            }
          res.redirect('/s601998023')
          });
        });
      }

};
controller.deleteNow=(req,res)=>{
const { id }=req.params;
req.getConnection((err,conn)=>{
  conn.query('Delete from jutarat where id= ?',[id],(err,s601998023s) =>{
    const  errorss= {errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }]}
if (err) {
  req.session.errors=errorss;
  req.session.success=false;
}
    console.log('s601998023s');
    req.session.success=true;
    req.session.topic="ลบสำเร็จ";
    res.redirect('/s601998023');
  });
});
};


controller.new =(req,res) => {
  req.getConnection((err,conn) => {
    conn.query("select * from saran",(err,sarandata)=> {

      res.render('s601998023/jutaratForm',{
        session: req.session,
        data:sarandata});
    });
  });
};





module.exports = controller;
