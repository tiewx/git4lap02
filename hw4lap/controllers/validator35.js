const { check } = require('express-validator');

exports.addvalidator = [check('sam',"sam-ไม่ถูกต้อง").not().isEmpty(),
                        check('saeoung',"saeoung-ไม่ถูกต้อง").isFloat(),
                        check('D631998035',"D631998035-ไม่ถูกต้อง").isInt()
                      ];

exports.editvalidator = [check('sam',"sam-ไม่ถูกต้อง").not().isEmpty(),
                        check('saeoung',"saeoung-ไม่ถูกต้อง").isFloat(),
                        check('D631998035',"D631998035-ไม่ถูกต้อง").isInt()
                      ];
