const { check } = require('express-validator');

exports.addValidator = [check('tle',"tle-ไม่ถูกต้อง").not().isEmpty(),
                        check('chaiboonrueang',"chaiboonrueang-ไม่ถูกต้อง").isFloat(),
                        check('D611998011',"D611998011-ไม่ถูกต้อง").isInt()
                      ];

exports.editValidator = [check('tle',"tle-ไม่ถูกต้อง").not().isEmpty(),
                        check('chaiboonrueang',"chaiboonrueang-ไม่ถูกต้อง").isFloat(),
                        check('D611998011',"D611998011-ไม่ถูกต้อง").isInt()
                      ];
