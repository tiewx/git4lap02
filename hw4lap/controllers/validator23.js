

const { check } = require('express-validator');

exports.addValidator = [check('ploy',"ploy_ไม่ถูกต้อง").not().isEmpty(),
                        check('ratchawong',"ratchawong_ไม่ถูกต้อง").isFloat(),
                        check('d601998023',"d601998023_ไม่ถูกต้อง").isInt(),
                        ];

exports.editValidator = [check('ploy',"ploy_ไม่ถูกต้อง").not().isEmpty(),
                        check('ratchawong',"ratchawong_ไม่ถูกต้อง").isFloat(),
                        check('d601998023',"d601998023_ไม่ถูกต้อง").isInt()
                      ];
