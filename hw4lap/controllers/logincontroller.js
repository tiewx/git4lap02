const controller = {};

const { validationResult } = require('express-validator');

controller.f = (req, res) => {
    res.render("loginform", { session: req.session });
};
/*
controller.login = (req, res) => {
  if(req.body.username == 'ce' && req.body.password == 'mirot'){
    req.session.user=req.body.username;
      res.redirect('/home');
    console.log('ok');
  }else{
    res.end('LOGIN ERROR')
    res.redirect('/');
  }
};
*/
controller.x = (req, res) => {
  if(req.session.user){
    res.render("home", { session: req.session });
  }else {
    res.redirect('/');
  }
};
controller.login = (req,res) => {
  const data=req.body;
  const errors = validationResult(req);
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      res.redirect('/');
    }else {
      req.session.success=true;
      req.session.topic="username หรือ password  ไม่ถูกต้อง";
      if(req.body.username == 'ce' && req.body.password == 'mirot'){
        req.session.topic="login เสร็จเรียบร้อย";
        req.session.user=req.body.username;
          res.redirect('/home');
        console.log('ok');
      }else{
        res.redirect('/');
      }

    }
};

controller.home = (req, res) => {
  if(req.session.user){
    res.render("home", { session: req.session });
  }else {
    res.redirect('/');
  }
};

controller.logout = (req, res) => {
  req.session.destroy();
  res.redirect('/');
};

module.exports = controller;
