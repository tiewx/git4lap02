const { check } = require('express-validator');//check คือ ชื่อฟังชั่นที่อยู่ใน express-validator

exports.addvalidator = [check('tiew',"tiew-ไม่ถูกต้อง").not().isEmpty(),
                        check('somwan',"somwan-ไม่ถูกต้อง").isFloat(),
                        check('D611998019',"D611998019-ไม่ถูกต้อง").isInt()
                      ];

exports.editvalidator = [check('tiew',"tiew-ไม่ถูกต้อง").not().isEmpty(),
                        check('somwan',"somwan-ไม่ถูกต้อง").isFloat(),
                        check('D611998019',"D611998019-ไม่ถูกต้อง").isInt()
                      ];
