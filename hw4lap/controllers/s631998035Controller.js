
const controller = {};
const { validationResult } = require('express-validator');


controller.list = (req,res) => {
  if(req.session.user){
    req.getConnection((err,conn) => {
      conn.query('SELECT s.id as sid , s.sam as ssam , s.saeoung as ssaeoung , s.D631998035 as sD631998035 , n.id as nid , n.ploy as nploy ,n.ratchawong as nratchawong,n.d601998023 as nd601998023 FROM noppadol as s left JOIN jutarat as n ON s.id_jutarat = n.id ;',(err,tb) => {
        if(err){
          res.json(err);
        }else {
          res.render('s631998035/s35index',{
            data:tb,
            session: req.session
        });
        }
      });
    });
  }else {
    res.redirect('/');
  }
};

controller.add = (req,res) => {
  const data=req.body;
  if (data.id_jutarat=="") {
    data.id_jutarat=null;
  }
  const errors = validationResult(req);
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      res.redirect('/s631998035/new');
    }else {
      req.session.success=true;
      req.session.topic="เพิ่มข้อมูลสำเร็จ";
      if(req.session.user){
        req.getConnection((err,conn) =>{
          conn.query('INSERT INTO noppadol set ?',[data],(err,tb) =>{
            if(err){
              res.json(err);
            }

            console.log(tb);
            res.redirect('/s631998035');
          });
        });
      }else {
        res.redirect('/');
      }
    }
};

controller.save =(req,res) =>{
  console.log(req.body);
  const data=req.body;
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('INSERT INTO noppadol set ?',[data],(err,tb) =>{
        if(err){
          res.json(err);
        }

        console.log(tb);
        res.redirect('/s631998035');
      });
    });
  }else {
    res.redirect('/');
  }
};

controller.delete =(req,res) =>{
  const {id} =req.params;
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('DELETE FROM noppadol WHERE id = ?',[id],(err,tb) =>{
        if(err){
          const  errorss= {errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }]}
          req.session.errors=errorss;
          req.session.success=false;
        }
        console.log(tb);
        req.session.success=true;
        req.session.topic="ลบสำเร็จ";
        res.redirect('/s631998035');
      });
    });
  }else {
    res.redirect('/');
  }
};

controller.delete2 =(req,res) =>{
  const {id} =req.params;
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('DELETE FROM noppadol WHERE id = ?',[id],(err,tb) =>{
        if(err){
          const  errorss= {errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }]}
          req.session.errors=errorss;
          req.session.success=false;
        }
        console.log(tb);
        req.session.success=true;
        req.session.topic="ลบสำเร็จ";
        res.redirect('/s631998035');
      });
    });
  }else {
    res.redirect('/');
  }
};

controller.del =(req,res) =>{
  const {id} =req.params;
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('SELECT s.id as sid , s.sam as ssam , s.saeoung as ssaeoung , s.D631998035 as sD631998035 , n.id as nid , n.ploy as nploy ,n.ratchawong as nratchawong FROM noppadol as s left JOIN jutarat as n ON s.id_jutarat = n.id HAVING sid= ?',[id],(err,tb) =>{
        if(err){
          res.json(err);
        }
        console.log(tb);

        res.render('s631998035/s35formDelete',{
                data:tb[0],
                session: req.session

        });
      });
    });
  }else {
    res.redirect('/');
  }
};

controller.del2 =(req,res) =>{
  const {id} =req.params;
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('SELECT s.id as sid , s.sam as ssam , s.saeoung as ssaeoung , s.D631998035 as sD631998035 , n.id as nid , n.ploy as nploy ,n.ratchawong as nratchawong FROM noppadol as s left JOIN jutarat as n ON s.id_jutarat = n.id HAVING sid= ?',[id],(err,tb) =>{
        if(err){
          res.json(err);
        }
        console.log(tb);
        res.render('s631998035/s35formDelete',{
                data:tb[0],
                session: req.session

        });
      });
    });
  }else {
    res.redirect('/');
  }
};


controller.edit =(req,res) =>{
  const {id} =req.params;
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM noppadol where id = ?',[id],(err,tb) =>{
        if(err){
          res.json(err);
        }
        console.log(tb);
        res.render('s631998035/s35form',{
                data:tb[0],
                session: req.session
        });
      });
    });
  }else {
    res.redirect('/');
  }
};


controller.edit2 =(req,res) =>{
  const {id} =req.params;
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM noppadol where id = ?',[id],(err,noppadol) =>{
        if(err){
          res.json(err);
        }
        conn.query('SELECT * FROM jutarat;',(err,jutarat) => {
          if(err){
            res.json(err);
          }

          res.render('s631998035/s35form',{
                  data:noppadol[0],
                  data2:jutarat,
                  idx:id,
                  session: req.session
          });
        });
      });
    });
  }else {
    res.redirect('/');
  }
};

controller.up = (req,res) => {
  const errors = validationResult(req);
  const {id} =req.params;
  const data=req.body;
  if (data.id_jutarat=="") {
    data.id_jutarat=null;
  }
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      if(req.session.user){
        req.getConnection((err,conn) =>{
          conn.query('SELECT * FROM noppadol where id = ?',[id],(err,tb) =>{
            if(err){
              res.json(err);
            }
            console.log(tb);
            res.render('s631998035/s35form',{
                    data:xx[0],
                    session: req.session
            });
          });
        });
      }else {
        res.redirect('/');
      }
    }else {
      req.session.success=true;
      req.session.topic="เพิ่มข้อมูลสำเร็จ";
      if(req.session.user){
        req.getConnection((err,conn) =>{
          conn.query('UPDATE noppadol SET ? WHERE id = ?',[data,id],(err,tb) =>{
            if(err){
              res.json(err);
            }
            console.log(tb);
            res.redirect('/s631998035');
          });
        });
      }else {
        res.redirect('/');
      }
    }
};

controller.up2 = (req,res) => {
  const errors = validationResult(req);
  const {id} =req.params;
  const data=req.body;
  if (data.id_jutarat=="") {
    data.id_jutarat=null;
  }
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      if(req.session.user){
        req.getConnection((err,conn) =>{
          conn.query('SELECT * FROM noppadol where id = ?',[id],(err,noppadol) =>{
            if(err){
              res.json(err);
            }
            conn.query('SELECT * FROM jutarat;',(err,jutarat) => {
              if(err){
                res.json(err);
              }
              res.render('s631998035/s35form',{
                      data:noppadol[0],
                      data2:jutarat,
                      idx:id,
                      session: req.session
              });
            });
          });
        });
      }else {
        res.redirect('/');
      }
    }else {
      req.session.success=true;
      req.session.topic="เพิ่มข้อมูลสำเร็จ";
      if(req.session.user){
        req.getConnection((err,conn) =>{
          conn.query('UPDATE noppadol SET ? WHERE id = ?',[data,id],(err,tb) =>{
            if(err){
              res.json(err);
            }
            console.log(tb);
            res.redirect('/s631998035');
          });
        });
      }else {
        res.redirect('/');
      }
    }
};

controller.update =(req,res) =>{
  const {id} =req.params;
  const data=req.body;
  if (data.id_jutarat=="") {
    data.id_jutarat=null;
  }
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('UPDATE noppadol SET ? WHERE id = ?',[data,id],(err,tb) =>{
        if(err){
          res.json(err);
        }
        console.log(tb);
        res.redirect('/s631998035');
      });
    });
  }else {
    res.redirect('/');
  }
};

controller.new =(req,res) =>{
  const data=null;
  if(req.session.user){
    res.render('s631998035/s35form',{
      data:data,
      session: req.session
    });
  }else {
    res.redirect('/');
  }
};

controller.new2 =(req,res) =>{
  const data=null;
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM jutarat;',(err,tb) => {
        res.render('s631998035/s35form',{
          data:null,
          data2:tb,
          session: req.session
        });
      });
    });
  }else {
    res.redirect('/');
  }
};

controller.newsx =(req,res) =>{
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM noppadol;',(err,teacherdata) => {
        res.render('studentForm',{data:teacherdata});
      });
    });
  }else {
    res.redirect('/');
  }
};

module.exports = controller;
