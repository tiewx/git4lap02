const express = require('express');
const router = express.Router();

const s35Controller = require('../Controllers/s631998035Controller');
const validator =require('../controllers/validator35');

router.get('/s631998035',s35Controller.list);
router.post('/s631998035/add',validator.addvalidator,s35Controller.add);
router.get('/s631998035/delete/:id',s35Controller.delete2);
router.get('/s631998035/del/:id',s35Controller.del2);
router.get('/s631998035/update/:id',s35Controller.edit2);
router.post('/s631998035/update/:id',validator.editvalidator,s35Controller.up2);
router.get('/s631998035/new',s35Controller.new2);
router.get('/s631998035/newx',s35Controller.newsx);

module.exports = router;
