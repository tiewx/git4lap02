const express = require('express');
const router = express.Router();

const sController = require('../Controllers/s611998019Controller');
const validator =require('../controllers/validator19');

router.get('/s611998019',sController.list);
//router.post('/s611998019/add',sController.save);
router.post('/s611998019/add',validator.addvalidator,sController.add);
router.get('/s611998019/delete/:id',sController.delete2);
router.get('/s611998019/del/:id',sController.del2);
router.get('/s611998019/update/:id',sController.edit2);
//router.post('/s611998019/update/:id',sController.update);
router.post('/s611998019/update/:id',validator.editvalidator,sController.up2);
router.get('/s611998019/new',sController.new2);

router.get('/s611998019/newx',sController.newsx);

module.exports = router;
