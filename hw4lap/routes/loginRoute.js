const express = require('express');
const router = express.Router();

const loginController = require('../controllers/logincontroller');
const validatorlogin =require('../controllers/validatorlogin');

router.get('/',loginController.f);
router.get('/:x',loginController.x);
router.post('/login',validatorlogin.loginvalidator,loginController.login);
router.get('/home',loginController.home);
router.get('/logout',loginController.logout);

module.exports = router;
