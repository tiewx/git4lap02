const express = require('express');
const router = express.Router();

const naruechaiController = require('../Controllers/s611998011Controller');
const validator11 =require('../controllers/validator11');

router.get('/s611998011',naruechaiController.list);
//router.post('/s611998019/add',naruechaiController.save);
router.post('/s611998011/add',validator11.addValidator,naruechaiController.add);
router.get('/s611998011/delete/:id',naruechaiController.delete2);
router.get('/s611998011/del/:id',naruechaiController.del2);
router.get('/s611998011/update/:id',naruechaiController.edit2);
//router.post('/s611998019/update/:id',naruechaiController.update);
router.post('/s611998011/update/:id',validator11.editValidator,naruechaiController.up2);
router.get('/s611998011/new',naruechaiController.new2);
router.get('/s611998011/newx',naruechaiController.newsx);

module.exports = router;
