const express = require('express');
const body = require('body-parser')
const cookie = require('cookie-parser');
const session = require('express-session');
const mysql = require('mysql');
const connection = require('express-myconnection');
const app = express();

app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(body.urlencoded(express.static('public')));
app.use(cookie());
app.use(session({ secret: '123456' }));
app.use(connection(mysql, {
    host: 'localhost',
    user: 'root',
    password: 'Alohomora',
    port: 3306,
    database: 'f2k2'
}, 'single'));

const loginRoute = require('./routes/loginRoute');
app.use('/', loginRoute);

const s6023Route = require('./routes/s601998023Route');
app.use('/', s6023Route);

const s611998011Route = require('./routes/s611998011Route');
app.use('/', s611998011Route);

const tiewRoute = require('./routes/s611998019Route');
app.use('/', tiewRoute);

const samRoute = require('./routes/s631998035Route');
app.use('/', samRoute);


app.listen(8081);
